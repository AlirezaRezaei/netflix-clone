export const GET_Trending_MOVIE = "GET_Trending_MOVIE";
export const MOVIE = "MOVIE";
export const START_LOADING = "START_LOADING";
export const END_LOADING = "END_LOADING";

export const getMovie = (url, name) => ({ type: GET_Trending_MOVIE, url, name });
export const movie = (movie, name) => ({ type: MOVIE, movie, name });
