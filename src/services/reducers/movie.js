import { MOVIE, END_LOADING, START_LOADING } from '../actions';

const InitialState = {
    loading: false,
    movie: [],
};

export default (state = InitialState, action) => {
    switch (action.type) {
        case MOVIE:
            return {
                ...state,
                movie: [...state.movie, { name: action.name, data: action.movie }]
            };
        case START_LOADING:
            return { ...state, loading: true };
        case END_LOADING:
            return { ...state, loading: false };
        default:
            return state;
    }
};
