import { call, put, takeEvery, all } from 'redux-saga/effects';
import fetchMovie from '../api';


function* watchersRootSaga(action) {
    yield takeEvery('GET_Trending_MOVIE', fetchData, 'MOVIE')
}

function* fetchData(type, action) {
    yield put({ type: 'START_LOADING' })
    try {
        const movie = yield call(fetchMovie, action.url);
        yield put({ type: type, movie: movie.data.results, name: action.name });
    } catch (e) {
        console.log("catch:", e)
    }
    yield put({ type: 'END_LOADING' })
}

export default function* mySaga() {
    yield all([
        watchersRootSaga()
    ])
}