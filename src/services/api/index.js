import axios from './axios';

async function fetchMovie(url) {
        const response =await axios.get(url);
        return response;
}

export default fetchMovie;