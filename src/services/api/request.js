const API_KEY = "72079747892c4c15ccaca5cc78d04ce3";

const request = {
    fetchTrending: `/trending/all/day?api_key=${API_KEY}`,
    fetchOriginal: `/discover/tv?api_key=${API_KEY}&with_networks=213`,
    fetchTopRated: `/movie/top_rated?api_key=${API_KEY}&language=en-US`,
}

export default request;