import React, { useEffect, useState } from 'react';
import Row from '../Row/Row';
import { connect } from 'react-redux';
import { getMovie } from '../../services/actions';
import request from '../../services/api/request';

function Netflix(props) {
    
    const [movie, setMovie] = useState()
    const [loading, setLoading] = useState()
    useEffect(() => {
        const { loading, movie } = props.movie;
        setMovie(movie);
        setLoading(loading);
    }, [props.movie])

    return (
        <div>
            <Row getMovie={props.getMovie} movie={movie} name="TRENDING" url={request.fetchTrending} isLarge />
            <Row getMovie={props.getMovie} movie={movie} name="ORIGINAL NETFLIX" url={request.fetchOriginal} />
            <Row getMovie={props.getMovie} movie={movie} name="TOP RATED" url={request.fetchTopRated} />
        </div>
    )

}

const mapStateToProps = state => ({
    movie: state.movie
})

const mapDispatchToProps = {
    getMovie: getMovie,
};

export default connect(mapStateToProps, mapDispatchToProps)(Netflix);