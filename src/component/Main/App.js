import React from 'react';
import {Provider} from 'react-redux';
import store from '../../services/store';
import Netflix from '../Netflix/Netflix';
import './App.css'

export default () =>(
  <Provider store={store}>
    <Netflix />
  </Provider>
);