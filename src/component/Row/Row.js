import React, { useEffect, useState } from "react";
import { imageUrl } from '../../services/api/baseUrl';
import './row.scss';

function Row(props) {
  const { getMovie, movie, name, url, isLarge } = props;
  const [movieCategory, setMovieCategory] = useState([])

  useEffect(() => {
    getMovie(url, name);
  }, [])

  useEffect(() => {
    setMovieData();
  }, [movie])

  async function setMovieData() {
    if (movie && movie.length > 0) {
      var found = await movie.find(function (element) {
        return element.name === name;
      });
      if (found) {
        setMovieCategory(found.data);
      }
    }
  }
  return (
    <div className="row" >
      <h2 className="row__title">{name}</h2>
      <List />
    </div>
  )

  function List() {
    if (!movieCategory) {
      //inja ezafe kn k y chizi return kone
      return null;
    }
    return (
      <div className="row__content">
        {movieCategory && movieCategory.map(item => (
          <div className="row__content__poster" key={item.id} >
            <img className={`row__content__poster__image ${isLarge && "row__content__poster__image--large-poster"}`} src={`${imageUrl}${isLarge ? item.backdrop_path : item.poster_path}`} alt={item.name} />
          </div>
        ))}
      </div>
    );
  }
}

export default Row;